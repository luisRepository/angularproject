import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.component.html',
  styleUrls: ['./sobre.component.scss']
})
export class SobreComponent implements OnInit {

  plataformas: Array<string> = ["Apple","Android","Windows Phone","BlackBerry"];

  constructor() { }

  ngOnInit() {
  }

}