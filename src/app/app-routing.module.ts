import { BlogComponent } from './blog/blog.component';
import { ContatoComponent } from './contato/contato.component';
import { SobreComponent } from './sobre/sobre.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanosComponent } from './planos/planos.component';

const routes: Routes = [
    { path: '', component: PlanosComponent },
    { path: 'sobre', component: SobreComponent },
    { path: 'blog', component: BlogComponent },
    { path: 'contato', component: ContatoComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
