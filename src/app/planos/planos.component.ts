import { PlanosService } from './planos.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-planos',
    templateUrl: './planos.component.html',
    styleUrls: ['./planos.component.scss']
})
export class PlanosComponent implements OnInit {

    planos: any

    constructor(
        private planosService: PlanosService
    ) { }

    ngOnInit() {
        this.buscarPlanos();
    }

    buscarPlanos() {
        this.planosService.buscarPlanos().subscribe(response => {
            this.planos = response;
        });
    }
}