import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API = environment.API;

@Injectable({
    providedIn: 'root'
})
export class PlanosService {

    constructor(
        private http: HttpClient
        ) { }

    buscarPlanos(): Observable<any> {
        return this.http.get<any>(`${API}/planos`);
    }
}
